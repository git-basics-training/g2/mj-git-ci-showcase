using NUnit.Framework;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace showcase.test
{
    public class CalcEngineTests
    {
        private CalcEngine _engine;
        [SetUp]
        public void Setup()
        {
            _engine = new CalcEngine();
        }

        [Test]
        public void OnePlusOne_should_return_two()
        {
            var x = 10;
            var y = 10;
            var expected = 20;

            var result = _engine.Add(x, y);

            Assert.AreEqual(expected, result);
        }
        [Test]
        public void OnePlusTwo_should_return_three()
        {
            var x = 1;
            var y = 2;
            var expected = 3;

            var result = _engine.Add(x, y);

            Assert.AreEqual(expected, result);
        }

        [Test]
        public void TwoPlusFive_should_return_seven()
        {
            var x = 2;
            var y = 5;
            var expected = 7;
        }

        [Test]
        public void OnePlusThree_should_return_four()
        {
            var x = 1;
            var y = 3;
            var expected = 4;

            var result = _engine.Add(x, y);

            Assert.AreEqual(expected, result);
        }

        [Test]
        public void OnePlusTwo_should_return_error()
        {
            var x = 1;
            var y = 2;
            var expected = 3;

            var result = _engine.Add(x, y);

            Assert.AreEqual(expected, result);
        }

        [Test]
        public void Epic_Success_Test()
        {
            var data = new System.Collections.Generic.List<double>() {1,2,3,4,5};
            double result = 0;
            double expected = 15;
            data.ForEach(x=> result = _engine.Add(result, x));

            Assert.AreEqual(expected, result);
        }
        [Test]
        public void OnePlusFour_should_return_five()
        {
            var x = 7;
            var y = 2;
            var expected = 9;

            var result = _engine.Add(x, y);

            Assert.AreEqual(expected, result);
        }

        [Test]
        public void OnePlusSix_should_return_error()
        {
            var x = 1;
            var y = 6;
            var expected = 5;

            var result = _engine.Add(x, y);

            Assert.AreNotEqual(expected, result);
        }

        [Test]
        public void OnePlusSix_should_return_seven()
        {
            var x = 1;
            var y = 6;
            var expected = 7;

            var result = _engine.Add(x, y);

            Assert.AreEqual(expected, result);
        }

        [Test]
        public void TwoPlusFive_should_return_Seven()
        {
            var x = 2;
            var y = 5;
            var expected = 7;

            var result = _engine.Add(x, y);

            Assert.AreEqual(expected, result);
        }
        [Test]
        public void TwoPlusTwo_should_return_error()
        {
            var x = 2;
            var y = 2;
            var expected = 4;

            var result = _engine.Add(x, y);

            Assert.AreEqual(expected, result);
        }
        [Test]
        public void TwoPlusTree_should_return_ss()
        {
            var x = 2;
            var y = 3;
            var expected = 5;

            var result = _engine.Add(x, y);
            Assert.AreEqual(expected, result);

        }
        [Test]
        public void TwoPlusTree_should_return_ss_error()
        {
            var x = 2;
            var y = 3;
            var expected = 5;

            var result = _engine.Add(x, y);
            Assert.AreEqual(expected, result);

        }
        [Test]
        public void PZOnePlusFour_should_return_five()
        {
            var y = 4;
            var x = 1;
            var expected = 5;

            var result = _engine.Add(x, y);

            Assert.AreEqual(expected, result);
        }
        
        [Test]
        public void ZeroPlusZero_should_return_zero()
        {
            var x = 0;
            var y = 0;
            var expected = 0;

            var result = _engine.Add(x, y);

            Assert.AreEqual(expected, result);
        }
    }
}